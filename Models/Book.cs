﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MyBookClub.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [DisplayName("Release Year")]
        [Range(1, 2022, ErrorMessage = "Display order must be between numbers 1 & 2022")]
        public int ReleaseYear { get; set; }
       
    }
}
